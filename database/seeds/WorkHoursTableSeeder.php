<?php

use Illuminate\Database\Seeder;

class WorkHoursTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('work_hours')->delete();
        
        \DB::table('work_hours')->insert(array (
            0 => 
            array (
                'work_id' => 1,
                'user_id' => 4,
                'work_type' => 1,
                'start_date' => '2018-06-04',
                'end_date' => '2018-06-04',
                'hours' => 1,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 17:57:21',
                'updated_at' => '2018-08-03 17:57:21',
            ),
            1 => 
            array (
                'work_id' => 2,
                'user_id' => 5,
                'work_type' => 1,
                'start_date' => '2018-08-03',
                'end_date' => '2018-08-03',
                'hours' => 8,
                'minutes' => 30,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-03 17:58:04',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            2 => 
            array (
                'work_id' => 3,
                'user_id' => 4,
                'work_type' => 1,
                'start_date' => '2018-05-21',
                'end_date' => '2018-05-21',
                'hours' => 3,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 17:58:17',
                'updated_at' => '2018-08-03 17:58:17',
            ),
            3 => 
            array (
                'work_id' => 4,
                'user_id' => 5,
                'work_type' => 1,
                'start_date' => '2018-08-02',
                'end_date' => '2018-08-02',
                'hours' => 1,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 17:58:29',
                'updated_at' => '2018-08-03 17:58:29',
            ),
            4 => 
            array (
                'work_id' => 5,
                'user_id' => 5,
                'work_type' => 3,
                'start_date' => '2018-09-01',
                'end_date' => '2018-09-01',
                'hours' => 4,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-03 18:03:31',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            5 => 
            array (
                'work_id' => 6,
                'user_id' => 4,
                'work_type' => 3,
                'start_date' => '2018-09-01',
                'end_date' => '2018-09-01',
                'hours' => 2,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:04:32',
                'updated_at' => '2018-08-03 18:04:32',
            ),
            6 => 
            array (
                'work_id' => 7,
                'user_id' => 4,
                'work_type' => 1,
                'start_date' => '2018-08-01',
                'end_date' => '2018-08-01',
                'hours' => 1,
                'minutes' => 30,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:05:35',
                'updated_at' => '2018-08-03 18:05:35',
            ),
            7 => 
            array (
                'work_id' => 8,
                'user_id' => 4,
                'work_type' => 1,
                'start_date' => '2018-07-30',
                'end_date' => '2018-07-30',
                'hours' => 1,
                'minutes' => 45,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-03 18:06:16',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            8 => 
            array (
                'work_id' => 9,
                'user_id' => 5,
                'work_type' => 4,
                'start_date' => '2018-09-04',
                'end_date' => '2018-09-21',
                'hours' => 112,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-03 18:11:16',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            9 => 
            array (
                'work_id' => 10,
                'user_id' => 4,
                'work_type' => 4,
                'start_date' => '2018-08-13',
                'end_date' => '2018-08-24',
                'hours' => 80,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-03 18:12:32',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            10 => 
            array (
                'work_id' => 11,
                'user_id' => 3,
                'work_type' => 1,
                'start_date' => '2018-07-24',
                'end_date' => '2018-07-24',
                'hours' => 4,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:13:48',
                'updated_at' => '2018-08-03 18:13:48',
            ),
            11 => 
            array (
                'work_id' => 12,
                'user_id' => 3,
                'work_type' => 1,
                'start_date' => '2018-08-06',
                'end_date' => '2018-08-06',
                'hours' => 0,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:14:20',
                'updated_at' => '2018-08-03 18:14:20',
            ),
            12 => 
            array (
                'work_id' => 13,
                'user_id' => 3,
                'work_type' => 1,
                'start_date' => '2018-08-21',
                'end_date' => '2018-08-21',
                'hours' => 5,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:15:38',
                'updated_at' => '2018-08-03 18:15:38',
            ),
            13 => 
            array (
                'work_id' => 14,
                'user_id' => 3,
                'work_type' => 3,
                'start_date' => '2018-09-01',
                'end_date' => '2018-09-01',
                'hours' => 9,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:15:59',
                'updated_at' => '2018-08-03 18:15:59',
            ),
            14 => 
            array (
                'work_id' => 15,
                'user_id' => 3,
                'work_type' => 4,
                'start_date' => '2018-08-21',
                'end_date' => '2018-08-21',
                'hours' => 5,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-03 18:16:57',
                'updated_at' => '2018-08-03 18:16:57',
            ),
            15 => 
            array (
                'work_id' => 16,
                'user_id' => 2,
                'work_type' => 1,
                'start_date' => '2018-07-23',
                'end_date' => '2018-07-23',
                'hours' => 2,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:36:01',
                'updated_at' => '2018-08-05 08:36:01',
            ),
            16 => 
            array (
                'work_id' => 17,
                'user_id' => 2,
                'work_type' => 1,
                'start_date' => '2018-07-25',
                'end_date' => '2018-07-25',
                'hours' => 1,
                'minutes' => 30,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:36:42',
                'updated_at' => '2018-08-05 08:36:42',
            ),
            17 => 
            array (
                'work_id' => 18,
                'user_id' => 2,
                'work_type' => 4,
                'start_date' => '2018-08-01',
                'end_date' => '2018-08-01',
                'hours' => 4,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:37:09',
                'updated_at' => '2018-08-05 08:37:09',
            ),
            18 => 
            array (
                'work_id' => 19,
                'user_id' => 2,
                'work_type' => 4,
                'start_date' => '2018-08-13',
                'end_date' => '2018-08-24',
                'hours' => 80,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:37:36',
                'updated_at' => '2018-08-05 08:37:36',
            ),
            19 => 
            array (
                'work_id' => 20,
                'user_id' => 2,
                'work_type' => 3,
                'start_date' => '2018-09-01',
                'end_date' => '2018-09-01',
                'hours' => 2,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:37:50',
                'updated_at' => '2018-08-05 08:37:50',
            ),
            20 => 
            array (
                'work_id' => 21,
                'user_id' => 1,
                'work_type' => 1,
                'start_date' => '2018-07-09',
                'end_date' => '2018-07-09',
                'hours' => 1,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-05 08:38:54',
                'updated_at' => '2018-08-05 08:43:21',
            ),
            21 => 
            array (
                'work_id' => 22,
                'user_id' => 1,
                'work_type' => 1,
                'start_date' => '2018-07-10',
                'end_date' => '2018-07-10',
                'hours' => 2,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:39:20',
                'updated_at' => '2018-08-05 08:39:20',
            ),
            22 => 
            array (
                'work_id' => 23,
                'user_id' => 1,
                'work_type' => 1,
                'start_date' => '2018-07-11',
                'end_date' => '2018-07-11',
                'hours' => 1,
                'minutes' => 30,
                'work_description' => NULL,
                'marked' => 0,
                'created_at' => '2018-08-05 08:39:57',
                'updated_at' => '2018-08-05 08:39:57',
            ),
            23 => 
            array (
                'work_id' => 24,
                'user_id' => 1,
                'work_type' => 4,
                'start_date' => '2018-07-13',
                'end_date' => '2018-07-13',
                'hours' => 4,
                'minutes' => 0,
                'work_description' => NULL,
                'marked' => 1,
                'created_at' => '2018-08-05 08:40:24',
                'updated_at' => '2018-08-05 08:43:21',
            ),
        ));
        
        
    }
}