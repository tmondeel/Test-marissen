<?php

use Illuminate\Database\Seeder;

class VacationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('vacations')->delete();
        
        \DB::table('vacations')->insert(array (
            0 => 
            array (
                'vacation_id' => 1,
                'user_id' => 1,
                'vacation_hours' => 200,
                'vacation_hours_left' => 196,
                'vacation_minutes_left' => 0,
                'created_at' => '2018-08-03 00:00:00',
                'updated_at' => '2018-08-05 08:40:24',
            ),
            1 => 
            array (
                'vacation_id' => 2,
                'user_id' => 2,
                'vacation_hours' => 200,
                'vacation_hours_left' => 116,
                'vacation_minutes_left' => 0,
                'created_at' => '2018-08-03 00:00:00',
                'updated_at' => '2018-08-05 08:37:36',
            ),
            2 => 
            array (
                'vacation_id' => 3,
                'user_id' => 3,
                'vacation_hours' => 200,
                'vacation_hours_left' => 195,
                'vacation_minutes_left' => 0,
                'created_at' => '2018-08-03 00:00:00',
                'updated_at' => '2018-08-03 18:16:57',
            ),
            3 => 
            array (
                'vacation_id' => 4,
                'user_id' => 4,
                'vacation_hours' => 200,
                'vacation_hours_left' => 120,
                'vacation_minutes_left' => 0,
                'created_at' => '2018-08-03 00:00:00',
                'updated_at' => '2018-08-03 18:12:32',
            ),
            4 => 
            array (
                'vacation_id' => 5,
                'user_id' => 5,
                'vacation_hours' => 200,
                'vacation_hours_left' => 88,
                'vacation_minutes_left' => 0,
                'created_at' => '2018-08-03 00:00:00',
                'updated_at' => '2018-08-03 18:11:16',
            ),
        ));
        
        
    }
}