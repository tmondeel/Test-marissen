<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'employee_1',
                'email' => 'employee_1@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => NULL,
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'employee_2',
                'email' => 'employee_2@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => 'aXc1rVMk3CnKGw7ri4rBpLrhv1CIqsW8uWPzCJZ9tjDEwJZxEYTDV7ik44Fy',
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'employee_3',
                'email' => 'employee_3@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => NULL,
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'employee_4',
                'email' => 'employee_4@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => 'mH4WWfPgQqRrfp7Ymk9CIWb05GBknvaZXlCu23FW3HzCoK1jJcJ6GAXQREbj',
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'employee',
                'email' => 'employee@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => '0JSgKImqoppfAv6rN8XDe6BfZs9fGGIFgZZpT4Ilwozrvr7UiREJtpIdeGdl',
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'manager',
                'email' => 'manager@notexisting.com',
                'password' => '$2y$10$i8i.mEcmIZ5dHM8L1LXiy.aMPdZ/MLDfhUOmn6GGtmjSiObI.Lhfa',
                'remember_token' => 'B076SkGYqRoHNAhYBFQbg28JHJDqiSYnfhfPS4O1aJgghMoL8nw7zx2jv8BT',
                'created_at' => '2018-08-03 14:30:26',
                'updated_at' => '2018-08-03 14:30:26',
            ),
        ));
        
        
    }
}