<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'role_id' => 1,
                'role_name' => 'employee',
                'role_description' => 'An employee is an worker of the company',
            ),
            1 => 
            array (
                'role_id' => 2,
                'role_name' => 'manager',
                'role_description' => 'An manager manages the employees for the company',
            ),
        ));
        
        
    }
}