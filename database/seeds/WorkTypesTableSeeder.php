<?php

use Illuminate\Database\Seeder;

class WorkTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('work_types')->delete();
        
        \DB::table('work_types')->insert(array (
            0 => 
            array (
                'work_type_id' => 1,
                'work_name' => 'Overtime',
                'work_description' => 'The time added as overtime',
            ),
            1 => 
            array (
                'work_type_id' => 2,
                'work_name' => 'Off Period',
                'work_description' => 'The time has been marked as off time and doesn`t have to ben paid out',
            ),
            2 => 
            array (
                'work_type_id' => 3,
                'work_name' => 'Pay out',
                'work_description' => 'Request to be paid out in this month',
            ),
            3 => 
            array (
                'work_type_id' => 4,
                'work_name' => 'Vacation Hours',
                'work_description' => 'These hours are from vacation and cann`t be paid out and are not apearing in the total overtime',
            ),
        ));
        
        
    }
}