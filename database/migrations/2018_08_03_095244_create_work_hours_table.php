<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkHoursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_hours', function(Blueprint $table)
		{
			$table->integer('work_id', true);
			$table->integer('user_id');
			$table->integer('work_type')->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->integer('hours');
			$table->integer('minutes');
			$table->text('work_description')->nullable();
			$table->boolean('marked')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_hours');
	}

}
