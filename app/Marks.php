<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $mark_id
 * @property string $mark_name
 * @property string $mark_description
 */
class Marks extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'mark_id';

    /**
     * @var array
     */
    protected $fillable = ['mark_name', 'mark_description'];

}
