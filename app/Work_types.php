<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $work_type_id
 * @property string $work_name
 * @property string $work_description
 */
class Work_types extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'work_type_id';

    /**
     * @var array
     */
    protected $fillable = ['work_name', 'work_description'];

}
