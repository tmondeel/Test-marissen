<?php

namespace App\Http\Middleware;

use App\User_roles;
use Closure;
use Illuminate\Support\Facades\Auth;

class Employee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*checks if the user has the correct role and redirects to home otherwise*/
    public function handle($request, Closure $next)
    {
    	$role = User_roles::where('user_id',Auth::user()->id)->first();
    	if ($role != null && $role->role_id != '1')
	    {
	    	return redirect('/home');
	    }
	    return $next($request);
    }
}
