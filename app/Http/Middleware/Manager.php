<?php

namespace App\Http\Middleware;

use Closure;
use App\User_roles;
use Illuminate\Support\Facades\Auth;
class manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next)
	{
		$role = User_roles::where('user_id',Auth::user()->id)->first();
		if ($role != null && $role->role_id != '2')
		{
			return redirect('/home');
		}
		return $next($request);
	}
	
}
