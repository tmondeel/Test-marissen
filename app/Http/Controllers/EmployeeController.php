<?php

namespace App\Http\Controllers;

use App\Vacations;
use App\Work_types;
use Illuminate\Http\Request;
use  App\Work_hours;
use Illuminate\Support\Facades\Auth;
use App\Traits\SharedFunctionTriat;
use Illuminate\Support\Facades\Redirect;
use DateTime;
class EmployeeController extends Controller {
	use SharedFunctionTriat;
	
	protected function time_and_type_check( $type, $time_used ) {
		//checks if the time is being deducted from the vacation days
		if ( $type == "vacation" ) {
			
			//send the user back in case the user wants to use more time than available
			if ( $this->check_and_update_vacationtime( $time_used ) == false ) {
				return false;
			}
			//setting the time back to hours
			$worktype = $this->get_worktype_id( 'Vacation Hours' );
			
			
		} //no vacation hours so it is the overtime hours
		else {
			//checks if the time available equal or larger than time used
			if ( $this->check_overtime( $time_used ) == false ) {
				return false;
				
			}
			//there is more time available than requested
			$worktype = $this->get_worktype_id( 'Off Period' );
		}
		
		return $worktype;
	}
	
	/**
	 * @param Request $request
	 * @param $time_used
	 *
	 * @return mixed
	 */
	
	private function validate_post_data( $data ) {
		return $data->validate( [
			'Time'      => 'required',
			'otimetype' => 'required'
		] );
	}
	//function that registers the overtime
	public function register_OverTime_post( request $request ) {
		$description = $request->has( 'description' ) ? $request->description : null;
		$time        = explode( ':', $request->Time );
		$worktype    = $this->get_worktype_id( "Overtime" );
		
		$this->register_Work_hours( [
			'work_type'        => $worktype,
			'start_date'       => $request->Date,
			'end_date'         => $request->Date,
			'hours'            => $time[0],
			'minutes'          => $time[1],
			'work_description' => $description,
		] );
		
		return redirect( '/home' );
	}
	
	public function time_off_post( request $request ) {
		$time = explode( ':', $request->Time );
		
		//time from the post to minutes
		$time_used = ( ( $time[0] * 60 ) + $time[1] );
		
		//gets the worktype and checks if the time is available
		$worktype = $this->time_and_type_check( $request->otimetype, $time_used );
		
		//checks if an error was found
		if ($worktype== false)
		{
			return Redirect::back()->withErrors( [ 'You cannot exceed the time available, 1 day equals 8 hours' ] );
		}
		//registering in the db
		$this->register_Work_hours( [
			'work_type'  => $worktype,
			'start_date' => $request->Date,
			'end_date'   => $request->Date,
			'hours'      => $time[0],
			'minutes'    => $time[1],
		] );
		
		return redirect( '/home' );
	}
	
	public function period_off_post( request $request ) {
		$days_used = $this->getWorkingDays( $request->From, $request->To );
		
		//time from the post to minutes
		$time_used = ( ( intval( $days_used ) * 8 ) * 60 );
		
		$worktype = $this->time_and_type_check( $request->otimetype, $time_used );
		//checks if an error was found
		if ($worktype== false)
		{
			return Redirect::back()->withErrors( [ 'You cannot exceed the time available, 1 day equals 8 hours' ] );
		}
		//registering in the db
		$this->register_Work_hours( [
			'work_type'  => $worktype,
			'start_date' => $request->From,
			'end_date'   => $request->To,
			'hours'      => intval( $days_used ) * 8,
			'minutes'    => '0'
		] );
		
		
		return redirect( '/home' );
	}
	
	public function payout_post( request $request ) {
		if ( $this->check_overtime( $request->Amount ) == false ) {
			return Redirect::back()->withErrors( [ 'You cannot exceed the time available' ] );
		}
		$worktype = $this->get_worktype_id( 'Pay out' );
		//adds the next month for the requested month
		$date = new DateTime("now");
		$date->modify( 'first day of next month' );
		$date =$date->format('Y-m-d');
		//registering in the db
		$this->register_Work_hours( [
			'work_type'  => $worktype,
			'start_date' => $date,
			'end_date'   => $date,
			'hours'      => $request->Amount,
			'minutes'    => '0'
		] );
		
		return redirect( '/home' );
	}
	
	
	public function register_overtime() {
		
		return view( 'register_overtime' );
	}
	
	public function time_off() {
		$vacation_time = $this->hours_and_minutes_to_day_Hours_minutes_calculator( $this->holidays_hours_left() );
		$over_Time     = $this->overHours_left();
		
		return view( 'register_time_off', [ 'vacation' => $vacation_time, 'overTime' => $over_Time ] );
	}
	
	public function period_off() {
		$over_Time     = $this->overHours_left();
		$vacation_time = $this->hours_and_minutes_to_day_Hours_minutes_calculator( $this->holidays_hours_left() );
		
		return view( 'register_periode_off', [ 'vacation' => $vacation_time, 'overTime' => $over_Time ] );
	}
	
	public function payout() {
		$over_Time     = $this->overHours_left();
		$vacation_time = $this->hours_and_minutes_to_day_Hours_minutes_calculator( $this->holidays_hours_left() );
		
		return view( 'register_payout_off', [ 'vacation' => $vacation_time, 'overTime' => $over_Time ] );
	}
}
