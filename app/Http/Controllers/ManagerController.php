<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\SharedFunctionTriat;
use App\Traits\ManagerFunctionTriat;

class ManagerController extends Controller {
	use SharedFunctionTriat;
	use ManagerFunctionTriat;
	
	public function mark_data( Request $request ) {
		$this->mark_rows( $request );
		
		return redirect( '/dashboard' );
	}
	
	public function dashboard() {
		$work_hours_rows = $this->get_all_Work_Hours_rows();
		$work_type_rows  = $this->get_all_work_types();
		$employee_data   = $this->overtime_and_vacation_all_employees();
		$employee_names  = $this->all_employees_names();
		if ( empty( $employee_names ) ) {
			dd( $employee_names );
		}
		
		return view( 'manager_dashboard', [ 'user_data'      => $employee_data,
		                                    'work_rows'      => $work_hours_rows,
		                                    'work_types'     => $work_type_rows,
		                                    'employee_names' => $employee_names
		] );
	}
}
