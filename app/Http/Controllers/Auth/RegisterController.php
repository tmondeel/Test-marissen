<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\User_roles;
use App\Vacations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    private function add_role($role_id,$user_id){
	    return User_roles::create([
		    'role_id' =>$role_id,
		    'user_id' =>$user_id
	    ]);
    }
    
    private function add_vacation($user_id,$vacation_hours){
    	return Vacations::create([
    		'user_id'=>$user_id,
		    'vacation_hours'=>$vacation_hours,
		    'vacation_hours_left'=>$vacation_hours,
		    'vacation_minutes_left'=>'0',
	    ]);
    }
    
    private function add_employee_defaults ($id){
        $this->add_role('1',$id);
        $this->add_vacation($id,'200');
    }
    
    private function add_manager_default($id){
	    $this->add_role('2',$id);
    }
    
    
    protected function create(array $data)
    {
    	
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        
	    ($data['UType'] == "manager")?$this->add_manager_default($user->id):$this->add_employee_defaults($user->id);
	    
	    return $user;
    }
}
