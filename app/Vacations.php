<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $vacation_id
 * @property int $user_id
 * @property int $year_start
 * @property int $year_end
 * @property int $vacation_hours
 * @property int $vacation_hours_left
 */
class Vacations extends Model
{
	public $timestamps = false;
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'vacation_id';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'year_start', 'year_end', 'vacation_hours', 'vacation_hours_left'];

}
