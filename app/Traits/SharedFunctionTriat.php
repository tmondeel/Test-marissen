<?php

namespace App\Traits;

use App\Marks;
use App\Vacations;
use App\Work_hours;
use App\Work_types;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait SharedFunctionTriat {
	
	//function that returns the id of the mark based on the name
	protected function get_worktype_id( $typename ) {
		$type = Work_types::all()->where( 'work_name', $typename )->first();
		
		return $type->work_type_id;
	}
	
	//function that returns the id of the mark based on the name
	protected function get_Mark_id( $markname ) {
		$mark = Marks::all()->where( 'mark_name', $markname )->first();
		
		return $mark->mark_id;
	}
	
	//function that colleckts all work types and send them back as an array
	protected function all_work_Types() {
		$types = Work_types::all();
		
		return $types->toArray();
	}
	
	protected function user_holiday_id() {
		return Vacations::all()->where( 'user_id', Auth::id() )->pluck( 'vacation_id' );
	}
	
	protected function holidays_hours_left($user='') {
		$minutes = '0';
		$hours = '0';
		if ($user == '')
		{
			$user = Auth::id();
		}
		
		$row = Vacations::all()->where( 'user_id', $user )->first();
		
		if ($row != null)
		{
			$hours = ($row->vacation_hours_left && $row->vacation_hours_left != null)?$row->vacation_hours_left:'0';
			$minutes = ($row->vacation_minutes_left && $row->vacation_minutes_left != null)?$row->vacation_minutes_left:'0';
		}
		
		return array( "Hours" => $hours, "Minutes" => $minutes );
	}
	
	protected function hours_and_minutes_to_day_Hours_minutes_calculator( $data ) {
		$vacation_days = ( $data['Hours'] / 8 );
		$hours         = 0;
		$minutes       = $data['Minutes'];
		//checks if there are rest hours
		if ( is_double( $vacation_days ) ) {
			$temp          = explode( ".", $vacation_days );
			$hours         = intval( ( $vacation_days - $temp[0] ) * 8 );
			$vacation_days = $temp[0];
			
		}
		
		return array( "Days" => $vacation_days, "Hours" => $hours, "Minutes" => $minutes );
	}
	
	private function time_array_to_Minutes( $array ) {
		$temp = $array->Hours * 60;
		$temp = ( $temp + $array->Minutes );
		
		return $temp;
	}
	
	protected function overHours_left($user = '') {
		// no fan of raw but it does the job
		//get the total Overhours, still the used hours need to be removed
		$user = ($user == '')?Auth::id():$user;
		$total_Time = Work_hours::query()
		                        ->select( DB::raw( "SUM(hours) as Hours, SUM(minutes) as Minutes" ) )
		                        ->where( 'work_type', $this->get_worktype_id( "Overtime" ) )
								->where('user_id',$user)
		                        ->get();
		//convert the time to minutes;
		$total_Time = $this->time_array_to_Minutes( $total_Time[0] );
//		DB::enableQueryLog();
		
		//get the used Time
		$total_used = Work_hours::query()
		                        ->select( DB::raw( "SUM(hours) as Hours, SUM(minutes) as Minutes" ) )
		                        ->where( 'work_type', '!=', $this->get_worktype_id( "Overtime" ) )
		                        ->where( 'work_type', '!=', $this->get_worktype_id( 'Vacation Hours' ) )
								->where('user_id',$user)
								->get();
//		dd(DB::getQueryLog());
		$total_used = $this->time_array_to_Minutes( $total_used[0] );
		$time_left  = ( $total_Time - $total_used );
		
		//gives the time in hours en minutes
		$time_left = ( $time_left / 60 );
		
		if ( is_double( $time_left ) ) {
			$temp    = explode( ".", $time_left );
			$hours   = $temp[0];
			$minutes = ( ( $time_left - $temp[0] ) * 60 );
		} else {
			$minutes = '0';
			$hours   = $time_left;
		}
		
		return array( "Hours" => $hours, "Minutes" => $minutes );
	}
	
	/*Function From Stackoverflow, slightly changed to fit for the usage*/
	protected function getWorkingDays( $startDate, $endDate ) {
		// do strtotime calculations just once
		$endDate   = strtotime( $endDate );
		$startDate = strtotime( $startDate );
		
		
		//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
		//We add one to inlude both dates in the interval.
		$days = ( $endDate - $startDate ) / 86400 + 1;
		
		$no_full_weeks     = floor( $days / 7 );
		$no_remaining_days = fmod( $days, 7 );
		
		//It will return 1 if it's Monday,.. ,7 for Sunday
		$the_first_day_of_week = date( "N", $startDate );
		$the_last_day_of_week  = date( "N", $endDate );
		
		//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
		//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
		if ( $the_first_day_of_week <= $the_last_day_of_week ) {
			if ( $the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week ) {
				$no_remaining_days --;
			}
			if ( $the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week ) {
				$no_remaining_days --;
			}
		} else {
			// (edit by Tokes to fix an edge case where the start day was a Sunday
			// and the end day was NOT a Saturday)
			
			// the day of the week for start is later than the day of the week for end
			if ( $the_first_day_of_week == 7 ) {
				// if the start date is a Sunday, then we definitely subtract 1 day
				$no_remaining_days --;
				
				if ( $the_last_day_of_week == 6 ) {
					// if the end date is a Saturday, then we subtract another day
					$no_remaining_days --;
				}
			} else {
				// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
				// so we skip an entire weekend and subtract 2 days
				$no_remaining_days -= 2;
			}
		}
		
		//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
		$workingDays = $no_full_weeks * 5;
		if ( $no_remaining_days > 0 ) {
			$workingDays += $no_remaining_days;
		}
		
		return $workingDays;
	}
	
	
	protected function register_Work_hours(
		$array = array(
			'work_type'   => 'Work_type',
			'Start_Date'  => '0000-00-00',
			'End_Date'    => '0000-00-00',
			'hours'       => '0',
			'minutes'     => '0',
			'Description' => null
		)
	) {
		$send_array = [
			'user_id'    => Auth::id(),
			'work_type'  => $array['work_type'],
			'start_date' => $array['start_date'],
			'end_date'   => $array['end_date'],
			'hours'      => $array['hours'],
			'minutes'    => $array['minutes'],
		];
		if ( in_array( 'Description', $array ) ) {
			$send_array .= [ 'work_description' => $array['Description'], ];
			dd( $send_array );
		}
		Work_hours::create( $send_array );
	}
	
	/**
	 * @param $time_left
	 */
	protected function update_vacation_reccord( $time_left ): void {
		//gets the currents user vacation reccord
		$reccord = Vacations::find( $this->user_holiday_id()[0] );
		
		//checking if the record is found
		if ( $reccord ) {
			if ( is_double( $time_left ) ) {
				$temp                           = explode( ".", $time_left );
				$hours                          = $temp[0];
				$minutes                        = ( ( $time_left - $temp[0] ) * 60 );
				$reccord->vacation_minutes_left = $minutes;
			}
			else
			{
				$hours = $time_left;
			}
			$reccord->vacation_hours_left = $hours;
			$reccord->save();
		}
	}
	
	protected function check_overtime( $time_used ) {
		$return              = true;
		$over_time_available = $this->overHours_left();
		$over_time_available = ( ( $over_time_available['Hours'] * 60 ) + $over_time_available['Minutes'] );
		//checking if the request is less or equal than the time available
		if ( $over_time_available < $time_used ) {
			$return = false;
		}
		
		return $return;
	}
	
	protected function check_and_update_vacationtime( $time_used ) {
		$return = true;
		// vtime == vacation time
		$Vacation_time = $this->holidays_hours_left();
		//available time in minutes
		$time_Available = ( ( $Vacation_time['Hours'] * 60 ) + $Vacation_time['Minutes'] );
		$time_left      = ( $time_Available - $time_used );
		
		//send the user back in case the user wants to use more time than available
		if ( $time_left < 0 ) {
			$return = false;
		} else {
			//updating the vacation record
			//setting the time back to hours
			$time_left = ( $time_left / 60 );
			$this->update_vacation_reccord( $time_left );
			
		}
		
		return $return;
	}
	
	protected function get_all_work_types()
	{
		return Work_types::all();
	}
	
	protected function payout_time ($user = ''){
		$user = ($user == '')?Auth::id():$user;
		
		$start = new Carbon('first day of next month');
		$end = new Carbon('last day of next month');
		$result = Work_hours::query()
		                 ->select( DB::raw( "SUM(hours) as Hours" ) )
		                 ->where( 'work_type', $this->get_worktype_id( "Pay out" ))
		                 ->whereBetween('start_date',array($start->toDateString(),$end->toDateString()))
		                 ->where('user_id',$user)
		                 ->get();
		
		$month = $start->format('F');
		$hours = ($result[0]->Hours == null)?'0':$result[0]->Hours;
		return array("Month"=>$month,"Hours"=>$hours);
	}
	
}
