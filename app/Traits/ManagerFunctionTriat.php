<?php

namespace App\Traits;

use App\User;
use App\User_roles;
use App\Work_hours;
use App\Marks;
use App\Vacations;
use App\Work_types;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait ManagerFunctionTriat {
	
	protected function all_employees_names() {
		$return_array = array();
		$employees    = $this->all_employees();
		foreach ( $employees as $employee ) {
			$name           = User::all()->where( 'id', $employee )->pluck( 'name' )[0];
			$return_array[] = array( "id" => $employee, "name" => $name );
		}
		
		return $return_array;
	}
	
	protected function all_employees() {
		return User_roles::all()->where( 'role_id', '1' )->pluck( 'user_id' );
	}
	
	protected function overtime_and_vacation_all_employees() {
		$data_array = array();
		$users      = $this->all_employees();
		
		foreach ( $users as $employee ) {
			$name           = User::all()->where( 'id', $employee )->pluck( 'name' )[0];
			$holiday_Data   = $this->hours_and_minutes_to_day_Hours_minutes_calculator( $this->holidays_hours_left( $employee ) );
			$overtime_left  = $this->overHours_left( $employee );
			$payout_request = $this->payout_time( $employee );
			$data_array[]   = array(
				"name"     => $name,
				"Holidays" => $holiday_Data,
				"Overtime" => $overtime_left,
				"payout"   => $payout_request
			);
		}
		
		return $data_array;
	}
	
	private function all_work_hours_query( $start_date, $end_date ) {
		return ( Work_hours::whereBetween( 'start_date', array(
			$start_date->toDateString(),
			$end_date->toDateString()
		) )
		                   ->orderBy( 'start_date', 'ASC' )
		                   ->orderBy( 'user_id', 'ASC' )
		                   ->orderBy( 'work_type', 'ASC' )
		                   ->get() );
	}
	
	protected function get_work_hours_by_month( $month ) {
		$start           = new Carbon( 'first day of ' . $month . ' month' );
		$end             = new Carbon( 'last day of ' . $month . ' month' );
		$last_month      = $this->all_work_hours_query( $start, $end );
		$last_month_name = $start->format( 'F' );
		
		return array( "Month" => $last_month_name, "Data" => $last_month );
	}
	
	
	protected function get_all_Work_Hours_rows() {
		$array_holder = array();
		
		$array_holder[] = $this->get_work_hours_by_month( 'last' );
		$array_holder[] = $this->get_work_hours_by_month( 'this' );
		$array_holder[] = $this->get_work_hours_by_month( 'next' );
		
		return $array_holder;
		
	}
	
	protected function mark_rows( $post_data ) {
		$all_id    = array();
		$post_data = $post_data->except( '_token' );
		foreach ( $post_data as $key => $value ) {
			$temp     = explode( '_', $key );
			$all_id[] = $temp[1];
		}
		Work_hours::whereIn( 'work_id', $all_id )
		          ->update( [
			          "marked" => '1'
		          ] );
	}
	
}
