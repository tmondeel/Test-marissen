<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $role_id
 * @property int $user_id
 */
class User_roles extends Model
{
	public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['role_id','user_id'];

}
