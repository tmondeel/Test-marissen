<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $work_id
 * @property int $user_id
 * @property int $work_type
 * @property string $start_date
 * @property string $end_date
 * @property int $hours
 * @property int $minutes
 * @property string $work_description
 * @property int $mark_id
 * @property string $created_at
 * @property string $updated_at
 */
class Work_hours extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'work_id';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'work_type', 'start_date', 'end_date', 'hours', 'minutes', 'work_description', 'mark_id', 'created_at', 'updated_at'];

}
