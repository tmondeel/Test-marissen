Installation instructions:

clone the project to an local directory  "git clone https://gitlab.com/tmondeel/Test-marissen.git"

go into the "Test-marissen" folder

//install the components
run from an commandline "composer install"

// adding the .env file
copy the .env.example with: (windows) "copy .env.example .env" or with bash "cp .env.example .env"

// adding the app key
add an app key "php artisan key:generate"

//adding the correct database connenction
open the .env file and file out DB_DATABASE, DB_USERNAME, DB_PASSWORD with your database table and login settings

//Migrate the database
after installation run on the command line "php artisan migrate"

//Seed the Database
run in the command line "php artisan db:seed"

all users have the password "marissen"
Employees login:
    employee@notexisting.com
    employee_1@notexisting.com
    employee_2@notexisting.com
    employee_3@notexisting.com
    employee_4@notexisting.com
    
Manager login: 
    manager@notexisting.com

//launch the application with "php artisan serve"