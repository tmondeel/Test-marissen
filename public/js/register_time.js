//alert('here');
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[id="Off Period"]').prop("checked", true);
    $('#types').val('Off Period');

    $('.Typer').change(function () {
         let checkbox = $(this).attr('data-item');
        $('#types').val(checkbox);
        if (checkbox == "Overtime")
         {
             $('#OtimeCheckbox').removeClass('hidden');
         }
         else
         {
             $('#OtimeCheckbox').addClass('hidden');
             $("#Nmonth").prop('checked', false);
         }
    });
});