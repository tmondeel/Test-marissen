<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
} );

Route::get( '/home', 'HomeController@index' )->name( 'home' );

Auth::routes();
Route::middleware( [ 'auth', 'employee' ] )->group( function () {
	
	Route::get( '/overtime_register', 'EmployeeController@register_OverTime' )->name( 'Register_OverTime' );
	Route::post( '/overtime_register', 'EmployeeController@register_OverTime_post' )->name( 'Register_OverTime_post' );
	
	Route::get( '/offtime_register', 'EmployeeController@time_off' )->name( 'time_off' );
	Route::post( '/offtime_register', 'EmployeeController@time_off_post' )->name( 'time_off_post' );

	Route::get( '/offperiod_register', 'EmployeeController@period_off' )->name( 'period_off' );
	Route::post( '/offperiod_register', 'EmployeeController@period_off_post' )->name( 'period_off_post' );

	Route::get( '/payout_register', 'EmployeeController@payout' )->name( 'payout' );
	Route::post( '/payout_register', 'EmployeeController@payout_post' )->name( 'payout_post' );
	
} );

Route::middleware( [ 'auth', 'manager' ] )->group( function () {
	
	Route::get( '/dashboard', 'ManagerController@dashboard' )->name( 'Dashboard' );
	Route::post( '/dashboard', 'ManagerController@mark_data' )->name( 'mark_data' );
} );