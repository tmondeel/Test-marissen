@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($errors->any())
                    <h4>{{$errors->first()}}</h4>
                @endif
                <div class="card">

                    <div class="card-header">Register days of absence</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('period_off_post') }}">
                            @csrf
<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-7 form-group">
        <label><b>Vacation:</b> {{$vacation['Days']}} <b>Days</b>, {{$vacation['Hours']}} <b>Hours,</b> {{$vacation['Minutes']}} <b>Minutes left</b> </label><br>
        <label> <b>Overhours Left:</b> {{$overTime['Hours']}} <b>Hours</b>, {{ round($overTime['Minutes'],0)}} <b>Minutes left</b> </label>
    </div>
</div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-md-right">Type of Hours used </label>
                                <div class="col-sm-6 row">
                                    <div class="radio col-sm-6">
                                        <label class="btn btn-info">
                                            <input type="radio" name="otimetype" id="Hours" value="Hours" required>
                                            Over Hours </label>
                                    </div>
                                    <div class="radio col-sm-6">
                                        <label class="btn btn-info">
                                            <input type="radio" name="otimetype" id="vacation" value="vacation">
                                            Vacation Days </label>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Date" class="col-sm-4 col-form-label text-md-right">{{ __('Start date') }}</label>

                                <div class="col-md-6">
                                    <input id="From" type="date"
                                           class="form-control{{ $errors->has('From') ? ' is-invalid' : '' }}"
                                           name="From" min="{{\Carbon\Carbon::now()->format('Y-m-d')}}" value="{{ (old('From')?old('From'):\Carbon\Carbon::now()->format('Y-m-d')) }}" required autofocus>

                                    @if ($errors->has('From'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('From') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Date" class="col-sm-4 col-form-label text-md-right">{{ __('End date') }}</label>

                                <div class="col-md-6">
                                    <input id="To" type="date"
                                           class="form-control{{ $errors->has('To') ? ' is-invalid' : '' }}"
                                           name="To" min="{{\Carbon\Carbon::now()->format('Y-m-d')}}" value="{{ (old('To')?old('To'):\Carbon\Carbon::now()->format('Y-m-d')) }}" required autofocus>

                                    @if ($errors->has('To'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('To') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" id="submitter" class="btn btn-primary">
                                        {{ __('Register days of absence') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{  asset('./js/period_off.js') }}"></script>
@endsection
