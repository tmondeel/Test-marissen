@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body row">
                        <div class="col-sm-12 col-md-6 border well pre-scrollable">
                            <form method="POST" action="{{ route('mark_data') }}">
                                @csrf

                                @foreach($work_rows as $month_rows)
                                    <div class="col-sm-12 wellV2">
                                        <div class="form-group row">

                                            {{--<label class="col-sm-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}
                                            <div class="col-sm-12"><h4 class="text-center">{{$month_rows['Month']}}</h4>
                                            </div>
                                            @foreach($month_rows['Data'] as $row)
                                                <div class="col-sm-12 form-group row">
                                                    @foreach($employee_names as $employee)
                                                        @if($employee['id'] == $row['user_id'])
                                                            <div class="col-sm-3">{{__($employee['name'])}}</div>
                                                            @break
                                                        @endif
                                                    @endforeach

                                                    <div class="col-sm-3">{{__($row['start_date'])}}</div>

                                                    @foreach($work_types as $work_type )
                                                        @if($work_type['work_type_id'] == $row['work_type'])
                                                            <div class="col-sm-3" data-toggle="tooltip"
                                                                 title="{{$work_type['work_description']}}">{{$work_type['work_name']}}</div>
                                                            @break

                                                        @endif
                                                    @endforeach
                                                        <div class="col-sm-2">{{round($row['hours'].'.'. round($row['minutes']/60,2),0)}}</div>


                                                    <div class="col-sm-1"><input type="checkbox" name="marker_{{$row['work_id']}}"
                                                                                 @if($row['marked'] == '1')
                                                                                 checked disabled

                                                                @endif
                                                        ></div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __("Mark entry's") }}
                                        </button>

                                    </div>
                                </div>

                            </form>

                        </div>
                        <div class="col-sm-12 col-md-6 border well pre-scrollable">
                            @foreach($user_data as $employee)
                                <div class="col-sm-12 wellV2">
                                    <div class="form-group row">

                                        {{--<label class="col-sm-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}
                                        <div class="col-sm-4"><h4 class="text-lg-left">{{__($employee["name"])}}</h4>
                                        </div>
                                        <div class="col-sm-7 form-group">
                                            <label><b>Vacation:</b> {{$employee['Holidays']['Days']}}
                                                <b>Days</b>, {{$employee['Holidays']['Hours']}}
                                                <b>Hours,</b> {{$employee['Holidays']['Minutes']}} <b>Minutes left</b>
                                            </label><br>
                                            <label> <b>Overhours Left:</b> {{$employee['Overtime']['Hours']}}
                                                <b>Hours</b>, {{ round($employee['Overtime']['Minutes'],0)}} <b>Minutes
                                                    left</b> </label>
                                            <br/>
                                            <br/>
                                            <label> <b>Payout in {{$employee['payout']['Month']}}
                                                    :</b> {{$employee['payout']['Hours']}} <b>Hours</b></label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
        });
    </script>
@endsection
