@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($errors->any())
                    <h4>{{$errors->first()}}</h4>
                @endif
                <div class="card">

                    <div class="card-header">Register amount to be paid out next month</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('payout_post') }}">
                            @csrf
<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-7 form-group">
        <label> <b>Overhours Available:</b> {{$overTime['Hours']}} <b>Hours</b></label>
    </div>
</div>
                            <div class="form-group row">
                                <label for="Ammount" class="col-sm-4 col-form-label text-md-right">{{ __('Hours') }}</label>

                                <div class="col-md-6">
                                    <input id="Amount" type="number" min="0" max="{{$overTime['Hours']}}"
                                           class="form-control{{ $errors->has('Amount') ? ' is-invalid' : '' }}"
                                           name="Amount" value="{{ old('Amount') }}" required autofocus>

                                    @if ($errors->has('Amount'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Amount') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Hours to be paid') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{  asset('./js/register_time.js') }}"></script>
@endsection
