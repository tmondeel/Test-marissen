@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($errors->any())
                    <h4>{{$errors->first()}}</h4>
                @endif
                <div class="card">

                    <div class="card-header">Register leave of absence</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('time_off_post') }}">
                            @csrf
<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-7 form-group">
        <label><b>Vacation:</b> {{$vacation['Days']}} <b>Days</b>, {{$vacation['Hours']}} <b>Hours,</b> {{$vacation['Minutes']}} <b>Minutes left</b> </label><br>
        <label> <b>Overhours Left:</b> {{$overTime['Hours']}} <b>Hours</b>, {{ round($overTime['Minutes'],0)}} <b>Minutes left</b> </label>
    </div>
</div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-md-right">Type of Hours used </label>
                                <div class="col-sm-6 row">
                                    <div class="radio col-sm-6">
                                        <label class="btn btn-info">
                                            <input type="radio" name="otimetype" id="Hours" value="Hours" required>
                                            Over Hours </label>
                                    </div>
                                    <div class="radio col-sm-6">
                                        <label class="btn btn-info">
                                            <input type="radio" name="otimetype" id="vacation" value="vacation">
                                            Vacation Days </label>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Date" class="col-sm-4 col-form-label text-md-right">{{ __('Date') }}</label>

                                <div class="col-md-6">
                                    <input id="Date" type="date"
                                           class="form-control{{ $errors->has('Date') ? ' is-invalid' : '' }}"
                                           name="Date" value="{{(old('Date')?old('Date'):\Carbon\Carbon::now()->format('Y-m-d')) }}" required autofocus>

                                    @if ($errors->has('Date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Time" class="col-sm-4 col-form-label text-md-right">{{ __('Time') }}</label>

                                <div class="col-md-6">
                                    <input id="Time" type="time"
                                           class="form-control{{ $errors->has('Time') ? ' is-invalid' : '' }}"
                                           name="Time" max="08:00" value="{{ old('Time') }}" step="900" required autofocus>

                                    @if ($errors->has('Time'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register leave of absence') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
