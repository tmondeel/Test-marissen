@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Register overtime</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('Register_OverTime_post') }}" >
                            @csrf


                            <div class="form-group row">
                                <label for="Date" class="col-sm-4 col-form-label text-md-right">{{ __('Date') }}</label>

                                <div class="col-md-6">
                                    <input id="Date" type="date"
                                           class="form-control{{ $errors->has('Date') ? ' is-invalid' : '' }}"
                                           name="Date" max="{{\Carbon\Carbon::now()->format('Y-m-d')}}" value="{{ (old('Date')?old('Date'):\Carbon\Carbon::now()->format('Y-m-d'))}}" required autofocus>

                                    @if ($errors->has('Date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Time" class="col-sm-4 col-form-label text-md-right">{{ __('Time') }}</label>

                                <div class="col-md-6">
                                    <input id="Time" type="time" max="12:00"
                                           class="form-control{{ $errors->has('Time') ? ' is-invalid' : '' }}"
                                           name="Time" value="{{ old('Time') }}" step="900" required autofocus>

                                    @if ($errors->has('Time'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                    <textarea id="description" name="description" class="btn give-border text-left"></textarea>

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register overtime') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
